//
// Created by anagu on 3/4/2021.
//
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct A{
    int a;
};

int main()
{
    struct A* memoryStart = (struct A*)malloc(sizeof(struct A));

    printf("Direccion inicial: %p\n", &memoryStart);

    clock_t chronoStart = clock();
    double sum = 0;
    double add = 1;

    unsigned long int iterations = 4294967295;
    for (unsigned int i = 0; i < iterations; i++)
    {
        sum+=add;
        add /=2.0;
    }

    clock_t chornoEnd = clock();
    double time = ((double)(chornoEnd-chronoStart))/CLOCKS_PER_SEC;

    printf("El tiempo es: %lf \n", time);

    struct A *memoryEnd = (struct A*)malloc(sizeof(struct A));
    printf("Direccion final:   %p\n", &memoryEnd);
    printf("Memoria (Direccion inicial - Direccion Final) : %ld \n", &memoryStart - &memoryEnd);
};

