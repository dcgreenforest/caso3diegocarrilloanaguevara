package main

import (
	"fmt"
	"time"
)

func main() {
	chronoStart := time.Now()
	var sum = 0
	var add = 1
	var iterations = 9223372036854775807
	for i := 1; i < iterations; i++ {

		sum += add
		add /= 2

	}
	chronoEnd := time.Since(chronoStart)

	fmt.Println(chronoEnd)

}
