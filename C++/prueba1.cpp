#include <iostream>
#include <ctime>
using namespace std;

#include "windows.h"
#include "psapi.h"
/**
 * UTILIZAMOS CTIME PARA MEDIR EL TIEMPO Y UN FOR QUE HACE EL MAXIMO DE ITERACIONES 
 * POSIBLES PARA UN UNSIGNED INT 
 * 
 * **/

struct A{
    int a;
};

int main()
{
    A *memoryStart = (A*)malloc(sizeof(A));
    cout << "Dirección de inicial: " << &memoryStart << endl;
    clock_t chronoStart = clock();
    double sum = 0;
    double add = 1;

    unsigned int iterations = 4294967295;
    for (unsigned int i = 0; i < iterations; i++)
    {
        sum+=add;
        add /=2.0;
    }

    clock_t chornoEnd = clock();
    double time = double (chornoEnd-chronoStart)/CLOCKS_PER_SEC;

    cout<<"El tiempo es: "<<time<<endl;
    A *memoryEnd = (A*)malloc(sizeof(A));
    cout << "Dirección final:   " << &memoryEnd << endl;
    cout << "Memoria (Dirección inicial - Dirección Final) : " << &memoryStart - &memoryEnd  << endl;

   /* MEMORYSTATUSEX memInfo;
    memInfo.dwLength = sizeof(MEMORYSTATUSEX);
    GlobalMemoryStatusEx(&memInfo);
    DWORDLONG virtualMemUsed = memInfo.ullTotalPageFile - memInfo.ullAvailPageFile;
    cout << "Memoria virtual total: " << memInfo.ullTotalPageFile / (1024 * 1024) << endl;
    cout << "Memoria virtual usada en mb: " << virtualMemUsed / (1024 * 1024) << endl;
    DWORDLONG physMemUsed = memInfo.ullTotalPhys - memInfo.ullAvailPhys;
    cout << "Memoria virtual total: " << memInfo.ullTotalPhys / (1024 * 1024) << endl;
    cout << "Memoria física usada (RAM) en mb:" << physMemUsed / (1024 * 1024)<< endl;
*/
   /* PROCESS_MEMORY_COUNTERS_EX pmc;
    GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
    SIZE_T virtualMemUsedByMe = pmc.PrivateUsage;
    cout << "Memoria usada por el proceso en mb:" << virtualMemUsedByMe / (1024 * 1024)<< endl;*/

};


