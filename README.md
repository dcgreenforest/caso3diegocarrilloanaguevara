# Caso#3

| Integrantes                  | Carnet     |
|------------------------------|------------|
| Diego Alonso Carrillo Arroyo | 2020182911 |
| Ana María Guevara Roselló    | 2018102514 |



### Pruebas realizadas:

Realizamos pruebas en una computadora de procesador Intel core i5 generación 8, cuya memoria ram es de 16 GB, utilizando el sistema operativo Windows 10.

El programa utilizado para probar los compiladores es una iteración de tamaño N en la que se le hace una suma al contador y luego este contador es divido entre 2.

Librerías usadas en C:
```
  #include <stdio.h>
  #include <time.h>
```
Librerías usadas en C++:
```
  #include <iostream>
  #include <ctime>
  using namespace std;
```
###Código en C y C++:
```
  clock_t chronoStart = clock();
  double sum = 0;
  double add = 1;

  unsigned int iterations = 4294967295;
  for (unsigned int i = 0; i < iterations; i++)
  {
      sum+=add;
      add /=2.0;
  }


  clock_t chornoEnd = clock();
  double time = ((double)(chornoEnd-chronoStart))/CLOCKS_PER_SEC;

  printf("El tiempo es: %lf", time);
```

Para las pruebas de C++ se utilizó el compilador de GNU(g++), mientras que para C se utilizó tcc.

| Pruebas\Lenguaje                         | C++                        | C                         | Go       | Rust            |
|------------------------------------------|----------------------------|---------------------------|----------|-----------------|
| unsigned int= 4,294,967,295              | 22.046 s                   | 21.977 s                  | 2.33 s   | 130 s           |
| long long int= 4,294,267,295 * 2         | 44.668 s                   | 21.862 s                  | 4.63 s   | 270 s           |
| long long int= 4,294,267,295 * 10        | 220.292 s                  | 21.633 s                  | 23.09 s  | 1360 s          |
| long long int= 9,223,372,036,854,775,807 | Indeterminado, +20 minutos | - | Indeterminado, +20 minutos | -     |


A continuación, los tiempos calculados para C++ y C respectivamente:
### Prueba # 1
![prueba#1 en C ](C/Img/ResultadoC_1.png)\
![prueba#1 en C++ ](C++/Img/ResultadoCpp_1.png)

### Prueba # 2

![prueba#2 en C ](C/Img/ResultadoC_2.png)
![prueba#2 en C++ ](C++/Img/ResultadoCpp_2.png)

### Prueba # 3

![prueba#3 en C ](C/Img/ResultadoC_3.png)
![prueba#3 en C++ ](C++/Img/ResultadoCpp_3.png)



### Código para Go:
```
  import (
  	"fmt"
  	"time"
  )
```
```
  chronoStart := time.Now()
  var sum = 0
  var add = 1
  var iterations = 4294967295
  for i := 1; i < iterations; i++ {

  	sum += add
  	add /= 2

  }
  chronoEnd := time.Since(chronoStart)

  fmt.Println(chronoEnd)
```

### Código para Rust:
```
  use std::time::{Duration, Instant};
```
```
  clock_t chronoStart = clock();
  double sum = 0;
  double add = 1;

  unsigned int iterations = 4294967295;
  for (unsigned int i = 0; i < iterations; i++)
  {
      sum+=add;
      add /=2.0;
  }


  clock_t chornoEnd = clock();
  double time = ((double)(chornoEnd-chronoStart))/CLOCKS_PER_SEC;

  printf("El tiempo es: %lf", time);
```

A continuación, los tiempos calculados para Go y Rust respectivamente:
### Prueba # 1
![prueba#1 en GO ](GO/Img/ResultadoGO_1.png)\
![prueba#1 en RUST ](RUST/Img/ResultadoRust_1.png)

### Prueba # 2

![prueba#2 en GO ](GO/Img/ResultadoGO_2.png)\
![prueba#3 en RUST ](RUST/Img/ResultadoRust_2.png)

### Prueba # 3

![prueba#3 en GO ](GO/Img/ResultadoGO_3.png)\
![prueba#3 en RUST ](RUST/Img/ResultadoRust_3.png)

###### Extra: Go utilizando 9,223,372,036,854,775,807 ... no hubo paciencia
![prueba#4 en GO ](GO/Img/ResultadoGO_4.png)

### Comparación de memoría:
En la comparación de memoria realizada entre C y C++ se obtuvieron estos resultados:

![prueba memoria](memoriaCvsCpp.png)
Esto se obtuvo modificando ligeramente el programa, obteniendo la dirección de memmoria del primer elemento utilizado (en  nuestro caso, chronoStart) y restándolo a la dirección de memoria del último elemento (chronoEnd)

```
    clock_t* pointStart, chronoStart;
    chronoStart = clock();
    pointStart = &chronoStart;
    cout << "Dirección de inicial: " << pointStart << endl;
    double sum = 0;
    double add = 1;

    unsigned int iterations = 4294967295;
    for (unsigned int i = 0; i < iterations; i++)
    {
        sum+=add;
        add /=2.0;
    }

    clock_t* pointEnd, chornoEnd;
    chornoEnd = clock();
    pointEnd = &chornoEnd;
    double time = double (chornoEnd-chronoStart)/CLOCKS_PER_SEC;

    cout<<"El tiempo es: "<<time<<endl;
    cout << "Dirección final:   " << pointEnd << endl;
    cout << "Memoria (Dirección inicial - Dirección Final) : " << pointStart - pointEnd  << endl;
```

### TOP 4
Comparando los tiempos durante las pruebas para cada lenguaje, el orden de menor a mayor tiempo es:
| Lenguaje En orden de rapidez |
|------------------------------|
| Go                           |
| C                            |
| C++                          |
| Rust                         |

Como conclusión , C y C++ tienen tiempos de ejecución muy similares, mientras que Go es el más eficiente de los 4 y según el algoritmo probado en este caso, Rust es el más deficiente.
