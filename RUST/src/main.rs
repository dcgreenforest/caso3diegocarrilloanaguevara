use std::time::{Duration, Instant};

fn main()
{
    let chronoStart = Instant::now();

    let iterations: u64   = 4294967295*10;
    let mut sum = 0;
    let mut add = 1;
    for _n in 0..iterations {
        sum = sum + add;
        add = add / 2;
    }
    println!("{}", chronoStart.elapsed().as_secs());
}